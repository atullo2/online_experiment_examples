/*
    This example shows how to load an external CSV file into jsPsych. Typically this
    would be used for something like a trial list or stimulus list.

    This also serves as an example varying
    styles in HTML.

    See also the experiment.html file which goes along with this one.
 */

/*
    These are global variables, i.e. the whole program can read and modify them.
*/

var stimuli_table_filename = "stimuli.csv";

/*
    If this is set to "true", the input list of stimuli will be truncated
    (see load_then_main).
*/
var debug = true;

/*
    The load_then_main function will store the parsed table from the CSV in it.
*/
var word_table;

/*
   Instead of just being written out as plain code, the code which runs the experiment
   is now in a function.
 */
function main()
{
    var timeline = [];
    var intro = {
        type: 'html-keyboard-response',
        stimulus: 'This is the example showing loading of trials from CSV'
    }
    timeline.push(intro);

    /*
        Here I use dynamic timeline variables (inside functions), this
        is necessary if you want to have anything other than just the variable
        on its own as part of a trial. See the jsPsych docs:
        https://www.jspsych.org/overview/timeline/#timeline-variables
        (specifically the second example and associated explanation)
    */
    var trial = {
        type: 'html-keyboard-response',
        /*
            As above: when using timeline variables in a complex way, put them
            in a function and remember to add the extra parameter (i.e.
            jsPsych.timelineVariable("colour",true)
            rather than
            jsPsych.timelineVariable("colour")
        */
        stimulus: function() {
            var html = (
                '<span style="color: '+jsPsych.timelineVariable("colour",true)+';'
                +'font-size: '+jsPsych.timelineVariable("size",true)+';'
                +'font-weight: bold; background-color: grey;'
                +'">'+jsPsych.timelineVariable("word",true)+'</span>'
            );
            return html;
        },
        // add colour / size / word as separate columns
        data: function() {
            return {
                colour: jsPsych.timelineVariable('colour',true),
                size: jsPsych.timelineVariable('size',true),
                word: jsPsych.timelineVariable('word',true)
            }
        }
    };

    var trials = {
        timeline: [trial],
        timeline_variables: word_table
    }
    timeline.push(trials);

    jsPsych.init({
        timeline: timeline,
        default_iti: 50, // inter-trial interval
        on_finish: function () {
            jsPsych.data.displayData("csv");
        }
    });
}

function load_then_main()
{
    /*
        Here we give "fetch" a link to the data. Because it's in the same
        directory as experiment.html, we can just use the filename.
        If we were loading it from elsewhere, we could use the folder name
        as well (on the same server) or a full URL for a different server
        (though most browsers complain about this for security reasons).
    */
    var promise = fetch(stimuli_table_filename, {method: 'get'}).then(
        // if the "fetch" is successful (CSV file loaded)
        function (response) {
            if (!response.ok) {
                /*
                    Make an error message (the brackets are just to split
                    it over multiple lines for readability) and show it.
                */
                var error_message = (
                    "Couldn't load "+stimuli_table_filename+"\n"
                    +"The experiment will not proceed."
                );
                window.alert(error_message);
                /*
                    This throws an error so the program ends, i.e. the
                    experiment doesn't start.
                */
                throw "Couldn't load data, exiting.";
            }
            return response.text().then(
                // once the response is converted to text ....
                function (text_in) {
                    // pass that text to papaparse
                    var parse_result = Papa.parse(
                        text_in,
                        {
                            delimiter: ',',
                            header: true,
                            skipEmptyLines: true
                        }
                    );
                    // store the result in word_table
                    word_table = parse_result.data;

                    // if we're in debug mode, shorten the table of trials
                    if (debug) {
                        word_table = word_table.splice(0, 5);
                    }
                }
            );
        }
    );
    promise.then(main);
}

load_then_main();
